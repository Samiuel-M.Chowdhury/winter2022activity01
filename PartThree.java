import java.util.Scanner;
public class PartThree {
	public static void main(String[] args) {
		Scanner keyboard=new Scanner(System.in);
		//Collect
		double firstValue = keyboard.nextDouble();
		double secondValue = keyboard.nextDouble();
		
		//Add
		double addition = Calculator.add(firstValue,secondValue);
		System.out.println("Addition: "+ addition);
		
		//Subtract
		double subtraction = Calculator.subtract(firstValue,secondValue);
		System.out.println("Subtraction: "+ subtraction);
		
		//Creating the instances
		Calculator calculate = new Calculator();
		
		//Multiply
		double multiplication = calculate.multiply(firstValue,secondValue);
		System.out.println("Multiplication: "+ multiplication);
		//Divide
		double division = calculate.divide(firstValue,secondValue);
		System.out.println("Division: "+ division);
	}

}
