public class MethodsTest {
	public static void main(String[] args) {
		int x=5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(x);
		
		System.out.println(x);
		methodTwoInputNoReturn(3,5.0);
		
		int returnedVariable=methodNoInputReturnInt();
		System.out.println("The returned variable is "+returnedVariable);
		
		double sqrtOfASum=sumSquareRoot(9,5);
		System.out.println("Here is the root of a sum: "+sqrtOfASum);
		
		String s1="java";
		String s2="programming";
		System.out.println("Length of s1 is: "+s1.length());
		System.out.println("Length of s2 is: "+s2.length());
		
		System.out.println("Calling methods from other class:");
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn() {
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x=20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int aNumber){
		aNumber=aNumber-5;
		System.out.println("Inside the method one input no return");
		System.out.println(aNumber);
		
	}
	public static void methodTwoInputNoReturn (int integer, double decimal) {
		System.out.println("MethodTwo");
		System.out.println(integer);
		System.out.println(decimal);
	}
	public static int methodNoInputReturnInt() {
		return 5;
	}
	public static double sumSquareRoot(int a, int b) {
		int sum=a+b;
		double result=Math.sqrt(sum);
		return result;
	}
}
